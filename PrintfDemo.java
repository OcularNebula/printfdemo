
public class PrintfDemo{
/* author: Zakhar Zakharevich
 * 2015
 * 
 * This program outputs formatted data as String, char, decimal int, exponential float, and float */

	public static void main(String[]args){
	//initialize variables
	String str1 = "fun";
	char chr1 = 'j';
	int int1 = 68;
	double pi = 3.14159265359;

	//String output
	System.out.printf("Java is super duper %s\n", str1);
	//char output
	System.out.printf("The best letter in the world is %c\n", chr1);
	//decimal int output
	System.out.printf("The best integer is %2d\n", int1);
	//exponential float output
	System.out.printf("The value of pi as exponential float is %2.2e\n", pi);
	//float output
	System.out.printf("The value of pi as float is %2.2f\n", pi);
	}
}
