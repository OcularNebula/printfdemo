import java.util.Scanner;

public class SumApp{
	public static void main(String[] args){

	//prompt user for 5 numbers
	System.out.println("Enter five(5) numbers separated by space");
	Scanner keyboard = new Scanner(System.in);
	double num1 = keyboard.nextDouble();
	double num2 = keyboard.nextDouble();
	double num3 = keyboard.nextDouble();
	double num4 = keyboard.nextDouble();
	double num5 = keyboard.nextDouble();

	//calculate sum and average
	double sum = (num1 + num2 + num3 + num4 + num5);
	double average = (sum / 5);

	//output calculation
	System.out.println("The sum of your numbers is " + sum + " and the average is " + average);
	}
}
